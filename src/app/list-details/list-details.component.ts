
import { Component } from '@angular/core';
import { List } from '../list/list.model';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
	selector: 'list-details-component',
	templateUrl: './list-details.component.html',
	styleUrls: ['./list-details.component.css']
})

export class ListDetails {

	list: List;

	constructor (private route: ActivatedRoute) {}

	ngOnInit () {
		console.log('ListDetails: Ready!');
		this.route.data.subscribe(
			(data: Data) => {
				this.list = data['list'];
			}
		)
	}

}