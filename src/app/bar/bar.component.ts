
import { Component, Input } from '@angular/core';

@Component({
	selector: 'bar-component',
	templateUrl: './bar.component.html',
	styleUrls: ['./bar.component.css']
})

export class BarComponent {

	@Input() id:number;	
	@Input() name:string;
	@Input() isOnline:boolean;
	status:string;

	constructor() { }

  ngOnInit() { 
		this.status = this.isOnline ? 'online' : 'offline';
	}
}
