import { Component } from '@angular/core';

import { AuthService } from './shared/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	
	title = 'Board';
	userAuthenticated: boolean;

	constructor(private authService: AuthService){}

	ngOnInit() {
		this.authService.userActivated.subscribe(
			(logInfo: boolean)=>{
				this.userAuthenticated = logInfo;
				console.log('AppComponent: Authenticated status - ' + this.userAuthenticated);
			}
		)
	}

	onLogIn() {
		this.authService.logIn();
	}
	onLogOut() {
		this.authService.logOut();
	}

}
