
import { Component } from '@angular/core';

import { List } from '../list/list.model';

import { DataService } from '../shared/database.service';

@Component({
	selector: 'lists-component',
	templateUrl: './lists.component.html',
	styleUrls: ['./lists.component.css'] 
})

export class ListsComponent {

	tempLists: List[] = [];

	constructor(private dataService: DataService) {}

	ngOnInit() {
		this.tempLists = this.dataService.getLists();
		this.dataService.refreshLists.subscribe(
			(lists:List[]) => {
				this.tempLists = lists;
			}
		);
	}

	onClose(listToDelete: List) {
		this.dataService.deleteList(listToDelete);
	}

}