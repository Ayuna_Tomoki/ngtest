import { Subject } from "rxjs";

export class AuthService {
	private loggedIn = false;
	userActivated = new Subject();

	isAuthenticated() {
		const promise = new Promise (
			(resolve, reject)=>{
				setTimeout(()=>{resolve(this.loggedIn)}, 5);
			}
		);
		return promise;
	}

	logIn() {
		this.loggedIn = true;
		this.userActivated.next(this.loggedIn);
		console.log('AuthService: Logged in!');
	}
	logOut() {
		this.loggedIn = false;
		this.userActivated.next(this.loggedIn);
		console.log('AuthService: Logged out!');
	}

}
