
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListsComponent } from '../lists/lists.component';
import { ListCreateComponent } from '../list-create/list-create.component';
import { ListDetails } from '../list-details/list-details.component';

import { ListResolver } from '../shared/list-resolver.service';
import { AuthGuard } from './auth-guard.service';
import { CanDeactivateGuard } from './deactivate-guard.service';

const appRoutes: Routes = [
	{path: '', redirectTo: '/lists', pathMatch: 'full'},
	{path: 'lists', component: ListsComponent},
	{path: 'list_create', component: ListCreateComponent, canActivate: [AuthGuard], canDeactivate:[CanDeactivateGuard]},
	{path: 'list_details/:list', component: ListDetails, resolve: {list: ListResolver}}
]

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [
		RouterModule
	]
	
})

export class AppRoutingModule {}