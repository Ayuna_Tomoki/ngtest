
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { List } from '../list/list.model';
import { DataService } from './database.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ListResolver implements Resolve<List> {

	constructor (private dataService: DataService) {}
	resolve (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<List> | Promise<List> | List {
		console.log('ListResolver: Sending requet to DataService...');
		return this.dataService.getList(+route.params['list']);
	}

}