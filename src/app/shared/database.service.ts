
import { Card } from '../card/card.model';
import { List } from '../list/list.model';
import { EventEmitter } from '@angular/core';

export class DataService {

	refreshLists = new EventEmitter<List[]>();
	sendList = new EventEmitter<List>();

	private lists: List[] = [
		new List('Thirst List', [
			new Card('To do 1', 'We reeeeally need something to do'),
			new Card('To do 2', 'We need something to do'),
			new Card('To do 5', 'We reeeeally need something')
		]),
		new List('Second List', [
			new Card('To do 3', 'We reeeeally need to do this'),
			new Card('To do 4', 'We need something to do'),
			new Card('To do 6', 'We reeeeally need something to do'),
			new Card('To do 7', 'We reeeeally need do')
		])
	];

	getLists() {
		console.log('DataService: Getting lists from base...');
		return this.lists.slice();
	}

	getList(listNumber: number): List {
		console.log('DataService: Getting list "' + this.lists[listNumber].listTitle + '" from base...');
		return this.lists[listNumber];
	}

	addNewList(title: string, cards: Card[]) {
		if(!title && !cards) {title='Default List'; cards = [new Card('Default Title','No information')]}
		this.lists.push(
			new List(title, cards)
		);
		console.log('DataService: Added new List "' + title + '" with ' + cards.length + ' cards...');
	}

	deleteList(list: List) {
		const index = this.getListIndexInLists(list);
		console.log('DataService: Deleted List "' + this.lists[index].listTitle + '"...');
		this.lists.splice(index, 1);
		this.refreshLists.emit(this.lists.slice());
		console.log('DataService: Emitted refreshed Lists...');
	}
	
	getListIndexInLists(list: List): number {
		return this.lists.indexOf(list);
	}
	
	createCards(amount: number): Card[] { 
		let cards: Card[] = [];
		for(let i=0; i<amount; i++) {
			cards.push(new Card('Card '+i, 'This is content for '+i));
			console.log('DataService: Created new "Card ' + i +'"...')
		}
		return cards;
	}
}
