
import { Component, Input } from '@angular/core';

import { Card } from './card.model';

@Component ({
	selector: 'card-component',
	templateUrl: './card.component.html',
	styleUrls: ['./card.component.css']
})

export class CardComponent {
	
	@Input() card: Card;
	
}