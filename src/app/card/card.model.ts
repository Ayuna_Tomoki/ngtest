
export class Card {
	public cardTitle: string;
	public cardContent: string;

	constructor(cardTitle: string, cardContent: string) {
		this.cardTitle = cardTitle;
		this.cardContent = cardContent;
	}
	
	
}
