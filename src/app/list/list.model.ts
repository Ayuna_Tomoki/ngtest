
import { Card } from '../card/card.model';

export class List {
	public listTitle: string;
	public listContent: Card[];

	constructor(listTitle: string, listContent: Card[]) {
		this.listTitle = listTitle;
		this.listContent = listContent;
	}
	
}
