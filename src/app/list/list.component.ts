
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { List } from './list.model';
import { DataService } from '../shared/database.service';

@Component({
	selector: 'list-component',
  templateUrl: './list.component.html',
	styleUrls: ['./list.component.css']
})

export class ListComponent {

	@Input() list:List;
	@Output() closeButtonPressed = new EventEmitter<void>();

	menuOpened: boolean = false;
	
	constructor(
		private dataService: DataService, 
		private router: Router
	) { }
	
	ngOnInit() {
		
	}

	getStyle(): string {
		if(this.menuOpened) {
			return 'side-button edit-button pressed'
		} else {
			return 'side-button edit-button'
		}
	}

	onDelete(){
		this.closeButtonPressed.emit();
	}
	onViewDetails(list: List) {
		const index = this.dataService.getListIndexInLists(list);
		this.onMenuButtonClick();
		this.router.navigate(['/list_details', index]);
	}
	onMenuButtonClick() {
		this.menuOpened = !this.menuOpened;
	}

}