
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

import { DataService } from '../shared/database.service';
import { CanComponentDeactivate } from '../shared/deactivate-guard.service';

import { Card } from '../card/card.model';

@Component({
	selector: 'list-create-component',
	templateUrl: './list-create.component.html',
	styleUrls: ['./list-create.component.css']
})

export class ListCreateComponent implements CanComponentDeactivate{

	constructor (private dataService: DataService, private router: Router) {}
	
	listCreateForm: FormGroup;
	cards: Card[] = [];
	canExit = false;

	ngOnInit() {
		this.listCreateForm = new FormGroup({
			'listName': new FormControl(null, Validators.required),
			'cards': new FormArray([])
		});
	}

	onAddCardForm() {
		const frmGroup = new FormGroup({
			'cardName': new FormControl(null, Validators.required),
			'cardText': new FormControl(null, Validators.required),
		});
		(<FormArray>this.listCreateForm.get('cards')).push(frmGroup);
		console.log(this.listCreateForm);
	}

	onDeleteCardForm(i:number) {
		const array = <FormArray>this.listCreateForm.controls['cards'];
		array.removeAt(i);
		console.log(i);
	}

	onSubmit() {
		const listTitle = this.listCreateForm.controls.listName.value;
		const cardsAmount = this.listCreateForm.controls.cards.value.length;

		for (let i=0; i<cardsAmount; i++) {
			let title: string = this.listCreateForm.controls.cards.value[i].cardName;
			let text: string = this.listCreateForm.controls.cards.value[i].cardText;
			this.cards.push(new Card(title, text));
		}
		
		this.dataService.addNewList(listTitle, this.cards.slice());
		this.canExit = true;
		this.router.navigate(['/lists']);
	}

	canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
		if ((!this.listCreateForm.dirty) || (this.listCreateForm.dirty && this.listCreateForm.valid && this.canExit)) {
			return true;
		} else {
			return confirm('Do you really want to exit and discard changes?');
		}
	}

}