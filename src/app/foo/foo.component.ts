
import { Component } from '@angular/core';
import { Bar } from '../bar/bar';

@Component({
	selector: 'foo-component',
	templateUrl: './foo.component.html',
	styleUrls: ['./foo.component.css']
})

export class FooComponent {

	counter: number = 0;
	bars: Bar[] = [];

	constructor() { }

	ngOnInit() { }
	
	addBar() {
		let status: boolean =	Math.random()<.5 ? false : true; 
		this.bars.push({id: this.counter, name: 'Bar number: '+this.counter, isOnline: status});
		this.counter++;
	}
}
