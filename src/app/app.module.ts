import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FooComponent } from './foo/foo.component';
import { BarComponent } from './bar/bar.component';

import { BoardComponent } from './board/board.component';
import { ListsComponent } from './lists/lists.component';
import { ListComponent } from './list/list.component';
import { ListDetails } from './list-details/list-details.component';
import { ListCreateComponent } from './list-create/list-create.component';
import { CardComponent } from './card/card.component';

import { DropdownDirective } from './shared/dropdown.directive';
import { AppRoutingModule } from './shared/app-routing.module';
import { ListResolver } from './shared/list-resolver.service';
import { DataService } from './shared/database.service';
import { AuthGuard } from './shared/auth-guard.service';
import { AuthService } from './shared/auth.service';
import { CanDeactivateGuard } from './shared/deactivate-guard.service';


@NgModule({
  declarations: [
		AppComponent,
		FooComponent,
		BarComponent,
		BoardComponent,
		ListsComponent,
		ListComponent,
		ListDetails,
		ListCreateComponent,
		CardComponent,
		DropdownDirective
  ],
  imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		ReactiveFormsModule
  ],
  providers: [
		ListResolver,
		DataService,
		AuthService,
		AuthGuard,
		CanDeactivateGuard
	],
  bootstrap: [AppComponent]
})
export class AppModule { }
